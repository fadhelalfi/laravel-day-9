<?php
require_once('animal.php');
require ('frog.php');
require ('ape.php');

$sheep = new animal("shaun");
echo "name : " . $sheep->name . "<br>";
echo "legs : " . $sheep->legs . "<br>";
echo "cold blooded : " . $sheep->cold_blooded . "<br> <br>";

$kodok = new frog("buduk");
echo "name : " . $kodok->name . "<br>";
echo "legs : " . $kodok->legs . "<br>";
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo $kodok->jump("Hop-Hop") . "<br> <br>";

$sungokong = new ape("kera sakti");
echo "name : " . $sungokong->name . "<br>";
echo "legs : " . $sungokong->legs . "<br>";
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo $sungokong->yell("auooo");



?>